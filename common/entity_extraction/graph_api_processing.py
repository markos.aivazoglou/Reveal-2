import text_analysis_freebase
from datetime import datetime, timedelta
import re
from recommender.models import User, Friend
# from pymongo import MongoClient
# from sentiment import analysis
from facebook import GraphAPI
import youtubeAPI
# from langid import classify
from rating_calculation import *


def get_user_history(user):
    my_movie_actions = []
    token = user['token']
    time_window = datetime.now() - timedelta(days=60)
    graph = GraphAPI(token)
    limit = 'limit(100)'
    links_fields = 'fields(link, id, name, created_time)'
    time_mod = 'since({0})'.format(str(time_window))
    links = 'links.{0}.{1}.{2}'.format(time_mod, links_fields, limit)
    video_sub = 'watches'
    video_fields = 'fields(id, data, publish_time)'
    video = 'video.{0}.{1}'.format(video_sub, video_fields)
    fields = {'fields': 'name,{0},{1}'.format(links, video)}
    my_wall = graph.get_object('me', **fields)
    user_movie_genres = user['movie_genres']

    my_links = (my_wall['links']['data'] if 'links' in my_wall else {})
    my_movie_actions = (my_wall['video.watches']['data']
                        if 'video.watches' in my_wall else {})

    for video in my_movie_actions:
        entity = {}
        video_data = video['data']
        if 'movie' in video_data:
            title = video_data['movie']['title']
            post_id = video['id']
            entity = text_analysis_freebase.search(title, "movie")
            if entity:
                genres = entity['genres']
                user_movie_genres = user['movie_genres']
                for genre in genres:
                    user_movie_genres[genre.lower()] = (user_movie_genres.get(
                                                        genre, 1)
                                                        + 1)
                if title:
                    movie_categories = {}
                    movie_categories['like_name'] = entity['name']
                    movie_categories['fb_id'] = post_id
                    movie_categories['genres'] = entity['genres']
                    user['movie_categories'].append(movie_categories)

    process_links_history(my_links, user)
    process_videos_history(my_movie_actions, user)
    total_weight_of_user_movie_genres = sum(user['movie_genres'].values())
    total_weight_of_user_music_genres = sum(user['music_genres'].values())

    user['total_movie_score'] = total_weight_of_user_movie_genres
    user['total_music_score'] = total_weight_of_user_music_genres
    user['movie_likes_score'] = get_score_sum(user['movie_categories'],
                                              user['movie_genres'])
    user['music_likes_score'] = get_score_sum(user['music_categories'],
                                              user['music_genres'])


def process_videos_history(my_movie_actions, user):
    for video in my_movie_actions:
        entity = {}
        video_data = video['data']
        user_movie_genres = user['movie_genres']
        if 'movie' in video_data:
            title = video_data['movie']['title']
            post_id = video['id']
            entity = text_analysis_freebase.search(title, "movie")
            entity_genres = (entity['genres'] if 'genres' in entity else None)
            for genre in entity_genres:
                user_movie_genres[genre] = user_movie_genres.get(genre, 1) + 1
            if title:
                movie_categories = {}
                movie_categories['like_name'] = entity['name']
                movie_categories['fb_id'] = post_id
                movie_categories['genres'] = entity_genres
                user['movie_categories'].append(movie_categories)


def process_links_history(my_links, user):
    for post in my_links:
        tids = []
        url = ""

        title = (post['name'] if 'name' in post else '')
        post_link = post['link']
        post_id = post['id']

        if any(element in post_link for element in ['youtube', 'youtu.be']):
            tids, url = youtubeAPI.getEntity(post_link, title)
        entity = {}

        for tid in tids:
            entity = text_analysis_freebase.link_search(tid, title)
            if entity:
                break
        if entity and entity['type'] == 'movie' and entity['genres']:
            entity_genres = entity['genres']
            user_movie_genres = user['movie_genres']
            for genre in entity['genres']:
                user_movie_genres[genre] = user_movie_genres.get(genre, 1) + 1
            movie_category = {}
            movie_category['like_name'] = entity['name']
            movie_category['fb_id'] = post_id
            movie_category['genres'] = entity['genres']
            user['movie_categories'].append(movie_category)
        elif entity and entity['type'] == 'music' and entity['genres']:
            entity_genres = entity['genres']
            user_music_genres = user['movie_genres']
            for genre in entity_genres:
                user_movie_genres[genre] = user_music_genres.get(genre, 1) + 1
            music_category = {}
            music_category['like_name'] = entity['name']
            music_category['fb_id'] = post_id
            music_category['genres'] = entity['genres']
            user['music_categories'].append(music_category)


def filter_likes(friend, token):
    friend_movie_likes = []
    friend_music_likes = []
    graph = GraphAPI(token)
    try:
        music_fields = 'id,name,category,genre'
        limit = 500
        friend_likes = graph.get_object(friend['id'] + '/music',
                                        fields=music_fields, limit=limit)
    except (Exception):
        friend_likes = {'data': []}
        friend_music_likes = []

    for like in friend_likes['data']:
        try:
            like['name'].encode('ascii')
        except(UnicodeDecodeError, UnicodeEncodeError):
            continue
        if 'Musician/band' in like['category']:
            like_object = {}
            like_object['id'] = like['id']
            like_object['name'] = like['name'].encode('utf-8')
            like_object['category'] = like['category']
            friend_music_likes.append(like_object)
    try:
        music_fields = 'id,name,genre,category'
        limit = 500
        friend_likes = graph.get_object(friend['id'] + '/movies',
                                        fields=music_fields, limit=limit)
    except:
        friend_likes = {'data': []}
        friend_movie_likes = []
    for like in friend_likes['data']:
        try:
            like['name'].encode('ascii')
        except (UnicodeDecodeError, UnicodeEncodeError):
            continue
        if 'Movie' in like['category'] and 'character' not in like['category']:
            like_object = {}
            like_object['id'] = like['id']
            like_object['name'] = like['name'].encode('utf-8')
            like_object['category'] = like['category']
            friend_movie_likes.append(like_object)
    return friend_movie_likes, friend_music_likes
