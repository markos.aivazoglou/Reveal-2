import text_analysis_freebase
from datetime import datetime, timedelta
import re
from recommender.models import User, Friend
from pymongo import MongoClient
from sentiment import analysis
from facebook import GraphAPI
import youtubeAPI
from langid import classify
from graph_api_processing import *
from rating_calculation import *


def create_user_profile(token):
    db_client = MongoClient('127.0.0.1')
    recomm = db_client.recommendation_db
    ins = recomm.users
    movie_likes = []
    music_likes = []
    movie_genres = {}
    music_genres = {}
    graph = GraphAPI(token)
    me = graph.get_object("me", fields='id,name')
    current = ins.find_one({'id': me['id']})  # Change to mongoengine
    if not current:

        user_likes = graph.get_object("me/movies",
                                      fields='id, name, category, genre',
                                      limit=500)
        user_likes_data = user_likes['data']

        for like in user_likes_data:
            like_category = like['category']
            if 'Movie' in like_category and 'character' not in like_category:
                movie_like = {}
                movie_like['id'] = like['id']
                movie_like['name'] = like['name'].encode('utf8')
                movie_like['category'] = like_category
                movie_likes.append(movie_like)

        user_likes = graph.get_object("me/music",
                                      fields='id, name, category, genre',
                                      limit=500)
        user_likes_data = user_likes['data']

        for like in user_likes_data:
            like_category = like['category']
            if 'Musician/band' in like_category:
                music_like = {}
                music_like['id'] = like['id']
                music_like['name'] = like['name'].encode('utf8')
                music_like['category'] = like['category']
                music_likes.append(music_like)

        movie_genres, movie_like_list = movie_gather(movie_likes)
        music_genres, music_like_list = music_gather(music_likes)

        total_weight_of_user_movie_genres = sum(movie_genres.values())
        total_weight_of_user_music_genres = sum(music_genres.values())
        user = User()
        user['movie_likes'] = movie_like_list
        user['movie_genres'] = movie_genres
        user['music_likes'] = music_like_list
        user['music_genres'] = music_genres
        user['total_movie_score'] = total_weight_of_user_movie_genres
        user['total_music_score'] = total_weight_of_user_music_genres
        user['movie_likes_score'] = get_score_sum(movie_like_list,
                                                  movie_genres)
        user['music_likes_score'] = get_score_sum(music_like_list,
                                                  music_genres)
        user['token'] = token
        user['has_lists'] = 0
        user['proc_posts'] = []
        if any([len(music_like_list), len(movie_like_list)]) < 10:
            get_user_history(user)
        return (movie_genres,
                music_genres,
                movie_like_list,
                music_like_list)
    else:
        return (current['movie_genres'],
                current['music_genres'],
                current['movie_likes'],
                current['music_likes'])


def create_interest_profile(user_movie_genres, user_music_genres,
                            user_movie_cat,
                            user_music_cat,
                            token):
    # Change to mongoengine calls
    client = MongoClient('127.0.0.1')
    db = client.recommendation_db
    friends = db.friends
    user = db.users
    results = []
    friend_movie_likes = []
    friend_music_likes = []
    friend_movie_genres = {}
    friend_music_genres = {}
    graph = GraphAPI(token)
    me = graph.get_object('me', fields='name,id')
    fields = 'id,name,picture'
    friends_list = graph.get_object('me/friends', fields=fields)
    try:
        friends_list = friends_list['data']
    except:
        pass
    user_buff = user.find_one({"id": me['id']}, {'total_movie_score': 'true',
                                                 'total_music_score': 'true',
                                                 'movie_likes': 'true',
                                                 'music_likes': 'true',
                                                 '_id': 'false',
                                                 'movie_likes_score': True,
                                                 'music_likes_score': True})
    for person in friends_list:
        friend = friends.find_one({'id': person['id']})  # TODO to mongoengine

        if (friend
           and friend['music_likes']
           and friend['movie_likes']):
            friend_movie_likes = friend['movie_likes']
            friend_music_likes = friend['music_likes']
            friend_movie_genres = friend['movie_genres']
            friend_music_genres = friend['music_genres']
            profile_picture = friend['picture']
        else:
            profile_picture = person['picture']['data']['url']
            (friend_movie_likes,
             friend_music_likes) = filter_likes(person, token)
            friend_movie_genres,
            friend_movie_likes = movie_gather(friend_movie_likes)
            friend_music_genres,
            friend_music_likes = music_gather(friend_music_likes)

        results = get_friend_scores(user_buff, person, user_movie_genres,
                                    user_movie_cat, friend_movie_genres,
                                    friend_movie_likes, user_music_genres,
                                    user_music_cat, friend_music_genres,
                                    friend_music_likes, profile_picture,
                                    results)
    user.update({'id': me['id']}, {'$set': {'scores': results}})
    return me['id']


def get_friend_scores(user, f,
                      user_movie_genres,
                      user_movie_cat,
                      friend_movie_genres,
                      friend_movie_likes,
                      user_music_genres,
                      user_music_cat,
                      friend_music_genres,
                      friend_music_likes,
                      profile_picture,
                      results):
    movie_genres_score = 0
    music_genres_score = 0
    movie_similarity = {}
    music_similarity = {}
    movie_likes_score = get_score_sum(friend_movie_likes, friend_movie_genres)
    music_likes_score = get_score_sum(friend_music_likes, friend_music_genres)

    friend_entry = {}
    if friend_movie_genres:
        movie_genres_score = sum(friend_movie_genres.values())
    if friend_music_genres:
        for mu_genres in friend_music_genres:
            music_genres_score = sum(friend_music_genres.values())

    # Inserting friend document to db
    # friend_todb = User()
    friend_todb = {}
    friend_todb['name'] = f['name']
    friend_todb['id'] = f['id']
    friend_todb['picture'] = profile_picture
    friend_todb['music_genres'] = friend_music_genres
    friend_todb['movie_genres'] = friend_movie_genres
    friend_todb['music_likes'] = friend_music_likes
    friend_todb['movie_likes'] = friend_movie_likes
    friend_todb['movie_likes_score'] = movie_likes_score
    friend_todb['music_likes_score'] = music_likes_score
    friend_todb['total_movie_score'] = movie_genres_score
    friend_todb['total_music_score'] = music_genres_score
    friend_todb['has_lists'] = 0
    friend_todb['proc_posts'] = []
    client = MongoClient('127.0.0.1')
    db = client.recommendation_db
    collection = db.friends
    collection.update({'id': f['id']}, friend_todb, True)

    friend_entry['friend_name'] = f['name']
    friend_entry['facebook_id'] = f['id']
    friend_entry['picture'] = profile_picture

    if friend_movie_likes:
        movie_similarity = get_movie_similarity(user_movie_genres,
                                                user_movie_cat,
                                                friend_movie_genres,
                                                friend_movie_likes,
                                                user,
                                                movie_genres_score,
                                                movie_likes_score)
        ovmovie_likes_score = (movie_similarity['item_score']
                               if (movie_likes_score
                               and movie_similarity['item_score']) != 0
                               else 0)
        ovmovie_genres_score = movie_similarity['genre_score']
        movie_friend_score = (float(2 * ovmovie_genres_score
                                    + ovmovie_likes_score)) / 3
        movie_similarity['movie_friend_score'] = int(round(movie_friend_score
                                                           * 100, 0))
    else:
        movie_similarity['movie_friend_score'] = 0

    if friend_music_likes:
        music_similarity = get_music_similarity(user_music_genres,
                                                user_music_cat,
                                                friend_music_genres,
                                                friend_music_likes,
                                                user,
                                                music_genres_score,
                                                music_likes_score)

        ovmusic_likes_score = (music_similarity['item_score']
                               if (music_likes_score
                               and music_similarity['item_score']) != 0
                               else 0)
        ovmusic_genres_score = music_similarity['genre_score']
        music_friend_score = (float(2 * ovmusic_genres_score
                                    + ovmusic_likes_score)) / 3
        music_similarity['music_friend_score'] = int(round(music_friend_score *
                                                           100, 0))
    else:
        music_similarity['music_friend_score'] = 0

    friend_entry['movie_score'] = movie_similarity
    friend_entry['music_score'] = music_similarity
    results.append(friend_entry)
    return results


def get_posts_and_create_lists(user_id):
    client = MongoClient('127.0.0.1')  # to mongoengine
    db = client.recommendation_db
    user_coll = db.users
    movies10 = []
    music10 = []
    query = user_coll.find_one({"id": user_id})
    user = query
    token = user['token']
    graph = GraphAPI(token)
    scores_list = user['scores']
    music_genres = user['music_genres']
    movie_genres = user['movie_genres']
    proc_posts = user['proc_posts']

    time_window = datetime.now() - timedelta(days=14)
    time_field = 'since({0})'.format(time_window)
    limit = 'limit(10)'

    comment_fields = 'fields(id,from,message)'
    comment_limit = 'limit(10)'
    comments = 'comments.{0}.{1}'.format(comment_fields, comment_limit)

    link_misc = 'message,link,id,name,created_time'
    link_fields = 'fields({0},{1})'.format(comments, link_misc)
    links = 'links.{0}.{1}.{2}'.format(time_field, link_fields, limit)

    status_misc = 'message,id,updated_time'
    status_fields = 'fields({0},{1})'.format(comments, status_misc)
    statuses = 'statuses.{0}.{1}.{2}'.format(time_field, status_fields, limit)

    video_fields = 'id,data,publish_time,message'
    video = 'video.watches.{0}'.format(video_fields)

    fields_of = '{0},name,pitcure,{1},{2}'.format(links, statuses, video)
    fields = {"fields": fields_of}

    for i in range(0, len(scores_list)):
        args = {}
        if (not scores_list[i]['music_score'] and
           not scores_list[i]['movie_score']):
            continue
        try:
            posts = graph.get_object(scores_list[i]['facebook_id'], **fields)
        except:
            continue

        links_filtered = (posts['links']['data'] if 'links' in posts
                          else [])

        statuses_filtered = (posts['statuses']['data'] if 'statuses'
                             in posts else [])

        videos = (posts['video.watches']['data'] if 'video.watches'
                  in posts else [])

        picture = posts['picture']['data']['url']

        for video in videos:
            published_time = video['publish_time']
            if 'movie' in video['data'] and published_time > str(ex):
                title = video['data']['movie']['title']
                post_id = video['id']
                if post_id not in proc_posts:
                    proc_posts.append(post_id)
                else:
                    continue
                entity = text_analysis_freebase.search(title, "movie")
                if entity:
                    genre_multiplier = 0
                    genres = entity['genres']
                    for genre in genres:
                        if (genre in movie_genres.keys()):
                                genre_multiplier += movie_genres[genre]
                    genre_multiplier = (float(genre_multiplier) / len(genres))
                    movie_score = (scores_list[i]['movie_score']
                                   ['movie_friend_score'])
                    score_now = (float(movie_score) * genre_multiplier)
                    title, url, description = youtubeAPI.getVideo(entity)
                    if title:
                        exist_flag = 0
                        for item in movies10:
                            if (((post_id == item["post_id"])
                                 and (url == item["embed"]))
                               or url == item["embed"]):
                                exist_flag = 1
                        if (exist_flag == 0):
                            item_object = {}
                            item_object['name'] = scores_list[i]['friend_name']
                            item_object['f_id'] = scores_list[i]['facebook_id']
                            item_object['post_id'] = post_id
                            item_object['score'] = score_now
                            item_object['picture'] = picture
                            item_object['title'] = title
                            item_object['embed'] = url
                            item_object['description'] = description
                            item_object['created'] = published_time[:10]
                            item_object['genres'] = genres
                            item_object['rated'] = 0
                            movies10.append(item_object)

        args['scores'] = scores
        args['movie'] = movie_genres
        args['music'] = music_genres
        args['pic'] = picture
        for link in links_filtered:
            if 'link' in link:
                process_link(link, proc_posts, movies10, music10, args)

        for status in statuses_filtered:
            if 'message' in status:
                lme = status['message']
                try:
                    lang = classify(lme)
                    if lang[0] != 'en':
                        continue
                except Exception as e:
                    print(e)
                    continue

                split_buffer = status['message'].split(" ")
                for word in split_buffer:
                    if 'http://' in word:
                        link_buffer = {}
                        link_buffer['id'] = status['id']
                        link_buffer['link'] = word
                        if 'updated_time' in status:
                            updated_time = status['updated_time']
                            link_buffer['created_time'] = updated_time
                            process_link(link_buffer, proc_posts, movies10,
                                         music10, args)
                        break
                    else:
                        process_status(status, proc_posts, movies10,
                                       music10, args)

    movies10 = sorted(movies10, key=lambda k: k['created'])
    movies10 = movies10[::-1]
    music10 = sorted(music10, key=lambda k: k['created'])
    music10 = music10[::-1]
    user_coll.update({"id": user_id},
                     {"$set": {"movies10": movies10,
                               "music10": music10,
                               "has_lists": 1,
                               "proc_posts": proc_posts}})


def process_link(link, proc_posts, movies10, music10, args):
    scores = args['scores']
    movie_genres = args['movie']
    music_genres = args['music']
    picture = args['pic']
    description = " "
    title = " "
    score_now = 0
    post_id = link['id']
    if post_id not in proc_posts:
        proc_posts.append(post_id)
    else:
        return -1
    message = link['message'] if 'message' in link else ''
    comments = link['comments']['data'] if 'comments' in link else ''
    created = link['created_time'][:10]
    title = link['name'] if 'name' in link else ''
    description = link['description'] if 'description' in link else ''
    post_link = link['link']
    if any(['youtube', 'youtube']) in post_link:
        topic_ids, url = youtubeAPI.getEntity(post_link, title)
        check = {}
        if topic_ids:
            final_score = 0
            message_score = (analysis.
                             sentiment_analysis(message.encode('utf8')))
            if message_score >= -0.1:
                final_score += 1
            else:
                final_score -= 1
            for comment in comments:
                if comment['from']['id'] == scores[i]['facebook_id']:
                    comment_score = 0
                    message = comment['message'].encode('utf8')
                    comment_score = analysis.sentiment_analysis(message)
                    if comment_score >= -0.1:
                        final_score += 1
                    else:
                        final_score -= 1
            if final_score < 0:
                return -1
            for tid in topic_ids:
                check = text_analysis_freebase.link_search(tid, title)
                if check:
                    break
        else:
            return -1
        entity = check
        if entity:
            genre_multiplier = 0
            if entity['type'] == 'movie' and entity['genres']:
                genres = entity['genres']
                for genre in genres:
                    if (genre in movie_genres.keys()):
                        genre_multiplier += movie_genres[genre]
                genre_multiplier = float(genre_multiplier) / len(genres)
                score_now = float(scores[i]['movie_score']
                                  ['movie_friend_score']) * genre_multiplier
                score_now = int(round(score_now, 0))
                exist_flag = 0
                for item in movies10:
                    if ((post_id == item["post_id"])
                       and (url == item["embed"])) or url == item["embed"]:
                        exist_flag = 1
                if (exist_flag == 0):
                    item_object = {}
                    item_object['name'] = scores_list[i]['friend_name']
                    item_object['f_id'] = scores_list[i]['facebook_id']
                    item_object['post_id'] = post_id
                    item_object['score'] = score_now
                    item_object['picture'] = picture
                    item_object['title'] = title
                    item_object['embed'] = url
                    item_object['description'] = description
                    item_object['created'] = published_time[:10]
                    item_object['genres'] = genres
                    item_object['rated'] = 0
                    movies10.append(item_object)
            elif entity['type'] == 'music' and entity['genres']:
                genres = entity['genres']
                for genre in genres:
                    if genre in music_genres.keys():
                        genre_multiplier += music_genres[genre]
                genre_multiplier = float(genre_multiplier) / len(genres)
                score_now = float(scores[i]['music_score']
                                  ['music_friend_score']) * genre_multiplier
                score_now = int(round(score_now, 0))
                exist_flag = 0
                for item in music10:
                    if ((post_id == item["post_id"])
                       and (url == item["embed"])) or url == item["embed"]:
                        exist_flag = 1
                if exist_flag == 0:
                    item_object = {}
                    item_object['name'] = scores_list[i]['friend_name']
                    item_object['f_id'] = scores_list[i]['facebook_id']
                    item_object['post_id'] = post_id
                    item_object['score'] = score_now
                    item_object['picture'] = picture
                    item_object['title'] = title
                    item_object['embed'] = url
                    item_object['description'] = description
                    item_object['created'] = published_time[:10]
                    item_object['genres'] = genres
                    item_object['rated'] = 0
                    music10.append(item_object)


def process_status(status, proc_posts, movies10, music10, args):
    scores = args['scores']
    movie_genres = args['movie']
    music_genres = args['music']
    picture = args['pic']
    title = ''
    description = ''
    score_now = 0
    post_id = status['id']
    if post_id not in proc_posts:
        proc_posts.append(post_id)
    else:
        return -1
    created = status['updated_time'][:10]

    message = status['message'] if 'message' in status else ''
    comments = status['comments']['data'] if 'comments' in status else ''
    final_score = 0
    message_score = analysis.sentiment_analysis(message.encode('utf-8'))
    if message_score >= -0.1:
        final_score += 1
    else:
        final_score -= 1
    for comment in comments:
        if comment['from']['id'] == scores[i]['facebook_id']:
            comment_score = 0
            comment_score = (analysis.
                             sentiment_analysis(c['message'].encode('utf8')))
            if comment_score >= -0.1:
                final_score += 1
            else:
                final_score -= 1
    if final_score < 0:
        return -1
    message = re.sub('\n', ' ', message)
    message = re.split('[ .,!?]', message)
    message = ' '.join(message[:8])
    results = text_analysis_freebase.getResults(message.encode('utf-8'))
    for entity in results:
        if entity:
            genre_multiplier = 0
            genres = entity['genres']
            if entity['type'] == 'movie' and genres:
                for genre in genres:
                    if (genre in movie_genres.keys()):
                        genre_multiplier += movie_genres[genre]
                genre_multiplier = float(genre_multiplier) / len(genres)
                score_now = float(scores[i]['movie_score']
                                  ['movie_friend_score']) * genre_multiplier
                if score_now > 0:
                    title, url, description = youtubeAPI.getVideo(entity)
                    if title:
                        exist_flag = 0
                        for it in movies10:
                            if ((post_id == it["post_id"])
                               and (url == it["embed"])) or url == it["embed"]:
                                exist_flag = 1
                        if (exist_flag == 0):
                            item_object = {}
                            item_object['name'] = scores_list[i]['friend_name']
                            item_object['f_id'] = scores_list[i]['facebook_id']
                            item_object['post_id'] = post_id
                            item_object['score'] = score_now
                            item_object['picture'] = picture
                            item_object['title'] = title
                            item_object['embed'] = url
                            item_object['description'] = description
                            item_object['created'] = published_time[:10]
                            item_object['genres'] = genres
                            item_object['rated'] = 0
                            movies10.append(item_object)
            elif entity['type'] == 'music' and entity['genres']:
                for key in entity['genres']:
                    if key in music_genres.keys():
                        genre_multiplier += music_genres[key]
                genre_multiplier = (float(genre_multiplier) /
                                    len(entity['genres']))
                score_now = float(scores[i]['music_score']
                                  ['music_friend_score']) * genre_multiplier
                if score_now > 0.0:
                    title, url, description = youtubeAPI.getVideo(entity)
                    if title:
                        exist_flag = 0
                        for it in music10:
                            if ((post_id == it["post_id"])
                               and (url == it["embed"])) or url == it["embed"]:
                                exist_flag = 1
                        if exist_flag == 0:
                            item_object = {}
                            item_object['name'] = scores_list[i]['friend_name']
                            item_object['f_id'] = scores_list[i]['facebook_id']
                            item_object['post_id'] = post_id
                            item_object['score'] = score_now
                            item_object['picture'] = picture
                            item_object['title'] = title
                            item_object['embed'] = url
                            item_object['description'] = description
                            item_object['created'] = published_time[:10]
                            item_object['genres'] = genres
                            item_object['rated'] = 0
                            music10.append(item_object)
