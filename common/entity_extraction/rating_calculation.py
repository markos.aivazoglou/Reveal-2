import text_analysis_freebase
from datetime import datetime, timedelta
import re
from recommender.models import User, Friend
from pymongo import MongoClient
from sentiment import analysis
from facebook import GraphAPI
import youtubeAPI
from langid import classify


def get_score_sum(likes, genres):  # Like's Average Score
    score = 0
    for like in likes:
        temp = sum(like['genres'].values())
        score += float(temp) / len(like['genres'])
    return score


def get_movie_similarity(user_movie_genres,
                         user_movie_likes,
                         friend_movie_genres,
                         friend_movie_likes,
                         user,
                         friend_genre_score,
                         friend_like_score):
    movie_similarity = {}
    genre_list = []
    item_list = []
    genre_score = 0
    item_score = 0
    genre_score, genre_list = get_genre_overlap(user_movie_genres,
                                                friend_movie_genres,
                                                user['total_movie_score'],
                                                friend_genre_score)
    for movies in user_movie_likes:
        for item in friend_movie_likes:
            if movies['like_name'] == item['like_name']:
                item_score += get_like_overlaps(user_movie_genres,
                                                friend_movie_genres,
                                                item['genres'])
                like_object = {}
                like_object['like_name'] = item['like_name']
                like_object['facebook_id'] = item['fb_id']
                item_list.append(like_object)  # list of overlapping items
                break
    item_score = round(float(item_score) / (user['movie_likes_score']
                                            + friend_like_score), 3)
    movie_similarity['genres'] = genre_list
    movie_similarity['genre_score'] = genre_score
    movie_similarity['items'] = item_list
    movie_similarity['item_score'] = item_score
    return movie_similarity


def get_music_similarity(user_music_genres, user_music_likes,
                         friend_music_genres, friend_music_likes,
                         user, friend_genre_score,
                         friend_like_score):
    music_similarity = {}
    genre_list = []
    item_list = []
    genre_score = 0
    item_score = 0
    genre_score, genre_list = get_genre_overlap(user_music_genres,
                                                friend_music_genres,
                                                user['total_music_score'],
                                                friend_genre_score)
    for music in user_music_likes:
        for item in friend_music_likes:
            if music['like_name'] == item['like_name']:
                item_score += get_like_overlaps(user_music_genres,
                                                friend_music_genres,
                                                item['genres'])
                like_object = {}
                like_object['like_name'] = item['like_name']
                like_object['facebook_id'] = item['fb_id']
                item_list.append(like_object)
            break

    item_score = round(float(item_score) / (user['music_likes_score']
                                            + friend_like_score), 3)
    music_similarity['genres'] = genre_list
    music_similarity['genre_score'] = genre_score
    music_similarity['items'] = item_list
    music_similarity['item_score'] = item_score
    return music_similarity


def get_genre_overlap(user_genres, friend_genres, user_score, friend_score):
    score, final_score = 0, 0
    genre_list = {}
    for genre in user_genres:
        if genre in friend_genres:
            score += user_genres[genre] + friend_genres[genre]
            genre_list[genre] = 
            genre_list[genre] = "{0} / {1}".format(str(friend_genres[genre]),
                                                   str(friend_score))
    final_score = round(float(score) / (user_score + friend_score), 3)
    return final_score, genre_list


def get_like_overlaps(user_genres, friend_genres, like_genres):
    score = 0
    user_genre_score = sum(user_genres.values())
    friend_genre_score = sum(friend_genres.values())
    score = round(((float(user_genre_score) / len(like_genres))
                  + (float(friend_genre_score) / len(like_genres))), 3)
    return score


def movie_gather(movie_likes):
    movie_genres = {}
    movie_likes = []
    for like in movie_likes:
        like_check = re.sub("[\\W]", " ", like['name'].strip())
        entity = text_analysis_freebase.search(like_check, "movie")
        if entity:
            genres = entity['genres']
            like_object = {}
            like_object['fb_id'] = like['id']
            like_object['fb_id'] = entity['title']
            like_object['fb_id'] = entity['genres']
            movie_likes.append(like_object)
            for genre in genres:
                movie_genres[genre] = movie_genres.get(genre, 1) + 1
    return movie_genres, movie_likes


def music_gather(music_likes):
    music_genres = {}
    music_likes = []
    for like in music_likes:
        genres = []
        entity = text_analysis_freebase.search(like['name'], "music")
        if entity:
            genres = entity['genres']
            like_object = {}
            like_object['fb_id'] = like['id']
            like_object['fb_id'] = entity['title']
            like_object['fb_id'] = entity['genres']
            music_likes.append(like_object)
            for genre in genres:
                music_genres[genre] = music_genres.get(genre, 1) + 1
    return music_genres, music_likes
