# from django.db import models
from mongoengine import Document, StringField, FloatField, DictField, ListField, IntField
# Create your models here.


class User(Document):
    name = StringField(max_length=255)
    fid = StringField(max_length=255)
    token = StringField(max_length=255)
    total_movie_score = FloatField()
    total_music_score = FloatField()
    movie_likes = ListField()
    music_likes = ListField()
    movie_genres = DictField()
    music_genres = DictField()
    movies_score = FloatField()
    music_score = FloatField()
    scores = ListField()
    music10 = ListField()
    movies10 = ListField()
    has_lists = IntField()
    proc_posts = ListField()


class Friend(Document):
    name = StringField(max_length=255)
    fb_id = StringField(max_length=255)
    total_friend_weight = FloatField()
    movie_genres = DictField()
    music_genres = DictField()
    music_list = ListField()
    movies_list = ListField()
