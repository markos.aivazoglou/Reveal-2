from django.http import HttpResponse
from django.template import RequestContext, loader
from django.shortcuts import render
from django.views.generic import View
from recommender.models import User, Friend
# from fb_info import create_user_profile, create_interest_profile, get_posts_and_create_lists
from datetime import datetime, timedelta
from mongoengine.queryset.visitor import Q
import json
import urllib.parse
import requests as req
# from common.entity_extraction import youtubeAPI
from django.conf import settings


class Index(View):
    redirect_uri = 'https://localhost:8000/recommender/init/'
    scope = 'user_likes,user_friends'
    client_id = settings.FACEBOOK_APP_ID
    params = {'client_id': client_id, 'redirect_uri': redirect_uri, 'scope': scope}

    def get(self, request):
        print(self.redirect_uri)
        auth = req.get('http://www.facebook.com/dialog/oauth', params=self.params)
        oauthredirect = auth.url
        return render(request, 'pages/auth.html', {"auth": oauthredirect})


class HandleToken(View):
    app_id = settings.FACEBOOK_APP_ID
    secret = settings.FACEBOOK_APP_SECRET

    def get(self, request):
        code = request.GET['code']
        token = self.ask_token(code)
        uid = self.get_user_id(token)
        query = User.objects(fid=uid).only('fid').first()
        if query:  # Check if user exists
            request.session['id'] = query['id']
            return render(request, 'pages/index.html', query)

        uid = self.create_user(token, uid)
        request.session['id'] = uid  # save session
        music_ex = ['music_genres', 'music_categories']
        movie_ex = ['movie_genres', 'movie_categories']
        excludes = music_ex + movie_ex
        query = User.objects(fid=uid).exclude(*excludes)
        user_info = query.to_json()
        return render(request, 'pages/recap.html', {"user_info": user_info})

    def ask_token(self, code):
        redirect_uri = 'https://localhost:8000/recommender/init/'
        app_info = {'client_id': self.app_id,
                    'redirect_uri': redirect_uri,
                    'client_secret': self.secret,
                    'code': code}
        token_req = req.get('https://graph.facebook.com/oauth/access_token',
                            params=app_info)
        response = token_req.text
        access_token = urllib.parse.parse_qs(response)
        try:
            token = access_token['access_token'][0]
        except(KeyError):
            print("KEY ERROR")
            token = None
        return token

    def get_user_id(self, token):
        app_token_info = {'client_id': self.app_id,
                          'client_secret': self.secret,
                          'grant_type': 'client_credentials'}
        oauth_url = 'https://graph.facebook.com/oauth/access_token?'
        app_token_req = req.get(oauth_url, params=app_token_info)
        app_token = urllib.parse.parse_qs(app_token_req.text)
        access_token = app_token['access_token'][0]
        debug_params = {'input_token': token, 'access_token': access_token}
        debug_url = 'https://graph.facebook.com/debug_token?'
        debug_request = req.get(debug_url, params=debug_params)
        parsed = json.loads(debug_request.text)
        uid = parsed['data']['user_id']
        return uid

    def create_user(self, token, uid):
        user_from_friends = Friend.objects(fid=uid).first()
        if not user_from_friends:
            (user_movie_genres,
             user_music_genres,
             user_movie_categories,
             user_music_categories) = create_user_profile(token)
        else:
            user = User()
            user['token'] = token
            user['has_lists'] = 0
            user['proc_posts'] = []
            user['time'] = 0
            user.save()
            user_music_genres = user_from_friends['music_genres']
            user_music_categories = user_from_friends['music_categories']
            user_movie_genres = user_from_friends['movie_genres']
            user_movie_categories = user_from_friends['movie_categories']
        uid = create_interest_profile(user_music_genres,
                                      user_music_categories,
                                      user_movie_genres,
                                      user_movie_categories,
                                      token)
        return uid


class DisplayIndex(View):
    def get(self, request):
        return render(request, 'pages/index.html')


class DisplayIndexInit(DisplayIndex):
    list_flag = 0

    def get(self, request):
        uid = '520078670'
        music_ex = ['music_genres', 'music_categories']
        movie_ex = ['movie_genres', 'movie_categories']
        excludes = music_ex + movie_ex
        user_info = User.objects(fid=uid).exclude(*excludes)
        form = request.POST
        self.list_flag = user_info['has_lists']
        self.update_scores(uid, user_info['scores'], form)

        if not self.list_flag:
            get_posts_and_create_lists(uid)
        return render(request, 'pages/index.html')

    def update_scores(self, uid, friends_list, new_form):
        for friend in friends_list:
            cur_id = friend['facebook_id']
            new_movie = new_form.get('music_' + cur_id)
            new_music = new_form.get('movie_' + cur_id)
            if new_movie is not None:
                op = 'set__scores__$__movie_score__movie_friend_score'
                up = {op: new_movie}
                User.objects.filter(Q(fid=uid) &
                                    Q(scores__facebook_id=cur_id)).update(**up)
            if new_music is not None:
                op = 'set__scores__$__music_score__music_friend_score'
                up = {op: new_music}
                User.objects.filter(Q(fid=uid) &
                                    Q(scores__facebook_id=cur_id)).update(**up)


class DisplayMovies(View):
    time_window = 2
    time = 0

    def get(self, request):
        uid = '520078670'
        movies = User.objects(fid=uid).only('movies10').first()['movies10']
        ex = str(datetime.now() - timedelta(days=self.time_window))[:10]

        movies10 = filter(lambda x: x['created'] > ex, movies)
        movies10 = sorted(movies10, key=lambda k: k['score'])[::-1]
        if len(movies10) > 10:
            movies10 = movies10[:10]
        elif len(movies10) == 0:
            movies10 = self.get_movie(uid)
        kwargs = {'movies': movies10, 'time': self.time}
        return render(request, 'pages/fb_movies.html', **kwargs)

    def get_movie(self, uid):
        movie_genres = User.objects(fid=uid).only('movie_genres').first()
        movie_genres = movie_genres['movie_genres']
        score1 = {}
        score2 = {}
        movies10 = []
        if movie_genres and len(movie_genres) > 6:
            sorted_genres = sorted(movie_genres.items(),
                                   key=lambda x: x[1])[::-1][:6]
            score1 = dict(sorted_genres[:3])
            score2 = dict(sorted_genres[4:6])
            to_string1 = " ".join(score1.keys())
            to_string2 = " ".join(score2.keys())
            search_term1 = "{0} movie trailer".format(to_string1)
            search_term2 = "{0} movie trailer".format(to_string2)
            title, url, description = youtubeAPI.getVideo(search_term1)
            if title:
                auto_title = "Your list was empty,\nso this recommendation was\
                made automatically."
                auto = {"name": auto_title,
                        "score": 11,
                        'title': title,
                        'embed': url,
                        'description': description}
                movies10.append(auto)
            title, url, description = youtubeAPI.getVideo(search_term2)
            if title:
                auto_title = "Your list was empty,\nso this recommendation was\
                made automatically."
                auto = {"name": auto_title,
                        "score": 11,
                        'title': title,
                        'embed': url,
                        'description': description}
                movies10.append()

        return movies10


class DisplayMoviesWeek(DisplayMovies):
    time_window = 7
    time = 1


class DisplayMovies2(DisplayMovies):
    time_window = 14
    time = 2


class DisplayMusic(View):
    time_window = 2
    time = 0

    def get(self, request):
        uid = '520078670'
        music = User.objects(fid=uid).only('music10').first()['music10']
        """
        ATT!!:removed as current data is old
        # ex = str(datetime.now() - timedelta(days=self.time_window))[:10]
        # music10 = filter(lambda x:x['created'] > ex,music)
        """
        music10 = sorted(music, key=lambda k: k['score'])[::-1][:10]
        if len(music10) > 10:
            music10 = music10[:10]
        elif len(music10) == 0:
            music10 = self.get_music(uid)
        template = loader.get_template('pages/fb_music.html')
        kwargs = {"music": music10, "time": self.time}
        context = RequestContext(request, **kwargs)
        return HttpResponse(template.render(context))

    def get_music(self, uid):
        music_genres = User.objects(fid=uid).only('music_genres')
        music_genres = music_genres['music_genres']
        score1 = {}
        score2 = {}
        music10 = []
        if music_genres and len(music_genres) > 6:
            sorted_genres = sorted(music_genres.items(),
                                   key=lambda x: x[1])[::-1][:6]
            score1 = dict(sorted_genres[:3])
            score2 = dict(sorted_genres[4:6])
            to_string1 = " ".join(score1.keys())
            to_string2 = " ".join(score2.keys())
            search_term1 = "{0}".format(to_string1)
            search_term2 = "{0}".format(to_string2)
            title, url, description = youtubeAPI.getVideo(search_term1)
            if title:
                auto_title = "Your list was empty,\nso this recommendation was\
                made automatically."
                auto = {"name": auto_title,
                        "score": 11,
                        'title': title,
                        'embed': url,
                        'description': description}
                music10.append(auto)
            title, url, description = youtubeAPI.getVideo(search_term2)
            if title:
                auto_title = "Your list was empty,\nso this recommendation was\
                made automatically."
                auto = {"name": auto_title,
                        "score": 11,
                        'title': title,
                        'embed': url,
                        'description': description}
                music10.append(auto)
        return music10


class DisplayMusicWeek(DisplayMusic):
    time_window = 7
    time = 1


class DisplayMusic2(DisplayMusic):
    time_window = 14
    time = 2


class DisplayScores(View):

    def get(self, request):
        uid = '520078670'
        music_ex = ['music_genres', 'music_categories']
        movie_ex = ['movie_genres', 'movie_categories']
        excludes = music_ex + movie_ex
        query = User.objects(fid=uid).exclude(*excludes)
        user_info = query.to_json()
        template = loader.get_template('pages/recap.html')
        context = RequestContext(request, {"user_info": user_info})
        return HttpResponse(template.render(context))


class Rate(View):
    rating = 0
    boost = 0

    def get(self, request):
        uid = '520078670'
        genres = request.GET['genres']
        typ = request.GET['type']
        pid = request.GET['id']
        for genre in json.loads(genres):
            try:
                if typ == 'music':
                    op = {'set__music10__$__rated': self.rating}
                    User.objects.filter(Q(fid=uid) &
                                        Q(music10__post_id=pid)).update(**op)
                    whole = 'inc__music_genres__{0}'.format(genre)
                    User.objects(fid=uid).update(**{whole: self.boost})
                elif typ == 'movie':
                    op = {'set__movies10__$__rated': self.rating}
                    User.objects.filter(Q(fid=uid) &
                                        Q(movies10__post_id=pid)).update(**op)
                    whole = 'inc__movie_genres__{0}'.format(genre)
                    User.objects(fid=uid).update(**{whole: self.boost})
            except Exception as e:
                print(e)
        return HttpResponse('OK')


class PlusRate(Rate):
    rating = 1
    boost = 1


class MinusRate(Rate):
    rating = -1
    boost = -1


class ResetRatePlus(Rate):
    rating = 0
    boost = -1


class ResetRateMinus(Rate):
    rating = 0
    boost = 1
