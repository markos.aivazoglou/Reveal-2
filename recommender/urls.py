from django.conf.urls import url

from recommender import views

urlpatterns = [
    url(r'^$', views.Index.as_view(), name='Index'),
    url(r'^init/$', views.HandleToken.as_view(), name='HandleToken'),
    url(r'^main/$', views.DisplayIndex.as_view(), name='DisplayIndex'),
    url(r'^movies/$', views.DisplayMovies.as_view(), name='DisplayMovies'),
    url(r'^movies_week/$', views.DisplayMoviesWeek.as_view(), name='DisplayMoviesWeek'),
    url(r'^movies_2_weeks/$', views.DisplayMovies2.as_view(), name='DisplayMovies2'),
    url(r'^music/$', views.DisplayMusic.as_view(), name='DisplayMusic'),
    url(r'^music_week/$', views.DisplayMusicWeek.as_view(), name='DisplayMusicWeek'),
    url(r'^music_2_weeks/$', views.DisplayMusic2.as_view(), name='DisplayMusic2'),
    url(r'^init_main/$', views.DisplayIndexInit.as_view(), name='DisplayIndexInit'),
    url(r'^scores/$', views.DisplayScores.as_view(), name='DisplayScores'),
    url(r'^plus_rated/$', views.PlusRate.as_view(), name='PlusRate'),
    url(r'^minus_rated/$', views.MinusRate.as_view(), name='MinusRate'),
    url(r'^reset_minus/$', views.ResetRateMinus.as_view(), name='ResetRateMinus'),
    url(r'^reset_plus/$', views.ResetRatePlus.as_view(), name='ResetRatePlus'),
]
