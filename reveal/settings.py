
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import sys
import mongoengine

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append('../common')

mongoengine.connect('recommendation_db')
# AUTHENTICATION_BACKENDS = (
#     'mongoengine.django.auth.MongoEngineBackend',
# )

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'gk^9!k^b!cj7q0nl3kv$11m)vn=4y+95(30y^6l5^l(-dnax^('
SESSION_ENGINE = 'mongoengine.django.sessions'
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []
FACEBOOK_APP_ID = '272825289733922'
FACEBOOK_APP_SECRET = 'd35fb09edb1b0a1844f3d98745451927'
FACEBOOK_APPLICATION_NAMESPACE = 'reveal'
FACEBOOK_CANVAS_PAGE = 'https://apps.facebook.com/reveal_recommender'
# FACEBOOK_CANVAS_URL = 'https://social-recommendation.ics.forth.gr/social_recommendation/'
# Application definition

# AUTHENTICATION_BACKENDS = (
#     'django_mongoengine.auth.backends.MongoEngineBackend',
# )
INSTALLED_APPS = (
    'sslserver',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'recommender'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'reveal.urls'
# WSGI_APPLICATION = 'reveal.wsgi.application'


DATABASES = {
    # 'default':
    # {
    #     # 'ENGINE':''
    #     # 'django.db.backends.sqlite3',
    #     # 'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    # }
}

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


STATIC_URL = '/static/'
STATIC_ROOT = 'reveal/recommender/static/'
